<?php

namespace Drupal\filecache;

/**
 * DrupalFileCache class
 *
 * @fixme Implement opcache use.
 * @fixme Implement expiration lookup.
 */
class DrupalFileCache implements \DrupalCacheInterface {
  protected $bin;
  protected $directory;

  const FILE_MAX_LEN = 250;

  /**
   * Construct DrupalFileCache for specified cache bin.
   *
   * @param $bin
   *   Cache bin name.
   */
  function __construct($bin) {
    $this->bin = $bin;
    $this->directory = Helper::getDirectory();
  }

  /**
   * Make cache ID usable for file name.
   *
   * @param $cid
   *   Cache ID.
   * @return
   *   String that is derived from $cid and can be used as file name.
   */
  function makeFilePath($cid) {
    // Use urlencode(), but turn the
    // encoded ':' and '/' back into ordinary characters since they're used so
    // often. (Especially ':', but '/' is used in cache_menu.)
    // We can't turn them back into their own characters though; both are
    // considered unsafe in filenames. So turn : -> <space> and / -> ^
    $safe_cid = str_replace(array('%2F', '%3A'), array('^', ' '), urlencode($cid));
    if (strlen($safe_cid) > static::FILE_MAX_LEN) {
      // Exactly so that len($safe_cid) === static::FILE_MAX_LEN
      $prefix_len = static::FILE_MAX_LEN - 2 - 32;
      $prefix = substr($safe_cid, 0, $prefix_len);
      $suffix = substr($safe_cid, $prefix_len);
      $safe_cid = $prefix . '%_' . md5($suffix);
    }
    $filepath = "$this->directory/$this->bin/$safe_cid";
    return $filepath;
  }

  function registerExpiration($filepath, $expire) {
    if ($expire == CACHE_PERMANENT) {
      return TRUE;
    }
    elseif ($expire == CACHE_TEMPORARY) {
      $expire = 0;
    }
    return file_put_contents("$this->directory/_expire/$expire", "$filepath\n", FILE_APPEND|LOCK_EX);
  }

  function deleteExpiredUpTo($timestamp) {
    $dirlen = strlen($this->directory);
    foreach (glob("$this->directory/_expire/*") as $expiration_file_name) {
      if (
        !($fh = fopen($expiration_file_name, 'rb')) ||
        !flock($fh, LOCK_EX)
      ) {
        continue;
      }
      while (!feof($fh)) {
        $filepath = rtrim(fgets($fh), PHP_EOL);
        if (substr($filepath, 0, $dirlen) !== $this->directory) {
          // Ignore files outside of our directory.
          continue;
        }
        unlink($filepath);
      }
      // Empty the file so no other process uses it before deleted.
      ftruncate($fh, 0);
      fclose($fh);
      unlink($expiration_file_name);
    }
  }

  /**
   * Return data from the persistent cache. Data may be stored as either plain
   * text or as serialized data. cache_get will automatically return
   * unserialized objects and arrays.
   *
   * @param $cid
   *   The cache ID of the data to retrieve.
   * @return
   *   The cache or FALSE on failure.
   */
  function get($cid) {
    if (!is_string($cid)) {
      return FALSE;
    }

    $filepath = $this->makeFilePath($cid);
    $this->delete_flushed();

    // Use @ because cache entry may not exist
    $content = @file_get_contents($filepath);
    if ($content === FALSE) {
      return FALSE;
    }
    $cache = @unserialize($content);
    if ($cache === FALSE) {
      // we are in the middle of cache_set
      $fh = fopen($filepath, 'rb');
      if ($fh === FALSE) {
        return FALSE;
      }
      if (flock($fh, LOCK_SH) === FALSE) {
        fclose($fh);
        return FALSE;
      }
      $cache = @unserialize(@stream_get_contents($fh));
      if ($cache === FALSE ||
          flock($fh, LOCK_UN) === FALSE ||
          fclose($fh) === FALSE) {
        unlink($filepath); // remove broken file
        flock($fh, LOCK_UN);
        fclose($fh);
        return FALSE;
      }
    }

    // XXX Should reproduce the cache_lifetime / cache_flush_$bin logic
    $cache_flush = variable_get('filecache_flush_' . $this->bin, 0);
    if ($cache->expire != CACHE_TEMPORARY && // XXX how to handle this?
        $cache->expire != CACHE_PERMANENT &&
        ($cache->expire < REQUEST_TIME ||
         ($cache_flush && $cache->created < $cache_flush))) {
      unlink($filepath);
      return FALSE;
    }

    return $cache;
  }

  /**
   * Return data from the persistent cache when given an array of cache IDs.
   *
   * @param $cids
   *   An array of cache IDs for the data to retrieve. This is passed by
   *   reference, and will have the IDs successfully returned from cache
   *   removed.
   * @return
   *   An array of the items successfully returned from cache indexed by cid.
   */
  function getMultiple(&$cids) {
    $results = array();
    foreach ($cids as $cid) {
      $cache = $this->get($cid);
      if ($cache !== FALSE) {
        $results[$cid] = $cache;
      }
    }
    $cids = array_diff($cids, array_keys($results));
    return $results;
  }

  /**
   * Store data in the persistent cache.
   *
   * @param $cid
   *   The cache ID of the data to store.
   * @param $data
   *   The data to store in the cache. Complex data types will be automatically
   *   serialized before insertion.
   *   Strings will be stored as plain text and not serialized.
   * @param $expire
   *   One of the following values:
   *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
   *     explicitly told to using cache_clear_all() with a cache ID.
   *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
   *     general cache wipe.
   *   - A Unix timestamp: Indicates that the item should be kept at least until
   *     the given time, after which it behaves like CACHE_TEMPORARY.
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    if (!is_string($cid)) {
      return;
    }

    $filepath = $this->makeFilePath($cid);
    if (!Helper::ensure_directory($filepath)) {
      watchdog('filecache', 'Cannot create directory for %filename',
        array('%filename' => $filepath), WATCHDOG_CRITICAL);
      return;
    }

    // Open file for that entry, handling errors that may arise
    $fh = @fopen($filepath, 'r+b');
    if ($fh === FALSE) {
      // If file doesn't exist, create it with a+w permissions
      $fh = fopen($filepath, 'c+b');
      if ($fh !== FALSE) {
        if (!drupal_chmod($filepath)) {
          watchdog('filecache', 'Cannot chmod %filename',
                   array('%filename' => $filepath), WATCHDOG_CRITICAL);
          return;
        }
      }
      else {
        // most likely permission error - report it as critical error
        watchdog('filecache', 'Cannot open %filename',
                 array('%filename' => $filepath), WATCHDOG_CRITICAL);
        return;
      }
    }

    // Our safeguard for simultaneous writing in the same file
    if (flock($fh, LOCK_EX) === FALSE) {
      fclose($fh);
      return;
    }

    $cache = new \StdClass;
    $cache->cid = $cid;
    $cache->created = REQUEST_TIME;
    $cache->expire = $expire;
    $cache->data = $data;

    if (ftruncate($fh, 0) === FALSE ||
        fwrite($fh, serialize($cache)) === FALSE ||
        flock($fh, LOCK_UN) === FALSE ||
        fclose($fh) === FALSE ||
        $this->registerExpiration($filepath, $expire) === FALSE
    ) {
      // XXX should not happen -> cleanup
      unlink($filepath);
      flock($fh, LOCK_UN);
      fclose($fh);
      return;
    }
  }

  /**
   * Expire data from the cache. If called without arguments, expirable
   * entries will be cleared from the cache_page and cache_block bins.
   *
   * @param $cid
   *   If set, the cache ID to delete. Otherwise, all cache entries that can
   *   expire are deleted.
   * @param $wildcard
   *   If set to TRUE, the $cid is treated as a substring
   *   to match rather than a complete ID. The match is a right hand
   *   match. If '*' is given as $cid, the bin $bin will be emptied.
   */
  function clear($cid = NULL, $wildcard = FALSE) {
    global $user;

    // parts are shamelessy copied from includes/cache.inc

    if (empty($cid)) {
      if (variable_get('cache_lifetime', 0)) {
        // We store the time in the current user's $user->cache variable which
        // will be saved into the sessions bin by _drupal_session_write(). We then
        // simulate that the cache was flushed for this user by not returning
        // cached data that was cached before the timestamp.
        $user->cache = REQUEST_TIME;

        $cache_flush = variable_get('cache_flush_' . $this->bin, 0);
        if ($cache_flush == 0) {
          // This is the first request to clear the cache, start a timer.
          variable_set('cache_flush_' . $this->bin, REQUEST_TIME);
        }
        elseif (REQUEST_TIME > ($cache_flush + variable_get('cache_lifetime', 0))) {
          // Clear the cache for everyone, cache_lifetime seconds have
          // passed since the first request to clear the cache.
          $this->deleteExpiredUpTo(REQUEST_TIME);
          variable_set('cache_flush_' . $this->bin, 0);
        }
      }
      else {
        // No minimum cache lifetime, flush all temporary cache entries now.
        $this->deleteExpiredUpTo(REQUEST_TIME);
      }
    }
    else {
      if ($wildcard) {
        if ($cid == '*') {
          $this->delete_wildcard('');
        }
        else {
          $this->delete_wildcard($cid);
        }
      }
      elseif (is_array($cid)) {
        foreach ($cid as $one_cid) {
          $this->delete_one($one_cid);
        }
      }
      else {
        $this->delete_one($cid);
      }
    }
  }

  /**
   * Delete a single cache object.
   *
   * @param $cid
   *   Cache ID.
   */
  protected function delete_one($cid) {
    $filepath = $this->makeFilePath($cid);
    @unlink($filepath);
  }

  /**
   * List of all cache objects with specified prefix in their name.
   *
   * @param $cid_prefix
   *   Prefix for cache IDs to delete.
   * @return array
   */
  protected function all($cid_prefix = '') {
    $path_prefix = $this->makeFilePath($cid_prefix);
    $list = glob("$path_prefix*");
    return $list;
  }

  /**
   * Delete all cache objects witch specified prefix in their name.
   *
   * @param $cid_prefix
   *   Prefix for cache IDs to delete.
   */
  protected function delete_wildcard($cid_prefix) {
    foreach ($this->all($cid_prefix) as $filepath) {
      @unlink ($filepath);
    }
  }

  /**
   * Delete flushed cache entries.
   */
  protected function delete_flushed() {
    static $recursion = FALSE; // XXX how cache.inc survives this?
    if ($recursion) {
      return;
    }
    $recursion = TRUE;

    // Garbage collection necessary when enforcing a minimum cache lifetime.
    $cache_flush = variable_get('cache_flush_' . $this->bin, 0);
    if ($cache_flush && ($cache_flush + variable_get('cache_lifetime', 0) <= REQUEST_TIME)) {
      // Reset the variable immediately to prevent a meltdown in heavy load situations.
      variable_set('cache_flush_' . $this->bin, 0);
      // Time to flush old cache data
      $this->deleteExpiredUpTo($cache_flush);
    } // if $cache_flush

    $recursion = FALSE;
  }

  /**
   * Check if a cache bin is empty.
   *
   * A cache bin is considered empty if it does not contain any valid data for
   * any cache ID.
   *
   * @return
   *   TRUE if the cache bin specified is empty.
   */
  function isEmpty() {
    return count($this->all()) == 0;
  }

}
