<?php

namespace Drupal\filecache;


class Helper {
  public static function getDirectory() {
    $uri = variable_get('filecache_directory');
    if (!$uri) {
      return FALSE;
    }
    $realpath = drupal_realpath($uri);
    $prefix = variable_get('cache_prefix', self::getCachePrefix());
    return "$realpath/$prefix";
  }

  /**
   * Ensure directory exists.
   *
   * @param $filepath
   * @return bool
   */
  public static function ensure_directory($filepath) {
    $directory = dirname($filepath);
    // Use drupal dir mask, but prevent others from looking into secrets.
    $mode = variable_get('file_chmod_directory', 0775);
    $mode &= variable_get('filecache_chmod_directory_mask', 0770);

    return mkdir($directory, $mode, TRUE);
  }

  /**
   * @return string
   */
  public static function getCachePrefix() {
    global $databases;
    $db = $databases['default']['default'];
    $cache_prefix = "$db[driver]://$db[username]@$db[host]/$db[database]#$db[prefix]";
    $encoded_cache_prefix = urlencode($cache_prefix);
    return $encoded_cache_prefix;
  }
}
