= File Cache

This modules allows Drupal caches to be stored in files instead of
storing in database.

= Installation

Install and enable filecache module as usual.

Add the following lines to "settings.php".
Adjust the module path or cache directory if needed.

$conf['cache_default_class'] = '\Drupal\filecache\DrupalFileCache';
$conf['filecache_directory'] = 'temporary://filecache';

= Procedure for setting and getting cache entries

Todo: Move to code comment.

Each cache object is stored in a separate file. File name is
concatenated cache bin name, '-' character, and urlencode()-like
encoded cid. Written in code, it's "$cachebin-$urlencodedcid". File
content is serialized stdClass exactly as returned by cache_get(),
i.e. stdClass with properties cid, created, expire and data.

cache_set uses the following sequence of operations on file that
contains cache object:

1. take exclusive lock
2. truncate to zero size
3. write new file content
4. release exclusive lock

cache_get is designed to be as fast as possible. It just tries to read
serialized content. If content cannot be unserialized, then some other
page request is running cache_set. (From truncation of file to
completing write of new file content, the content of the file is
unserializable.) In this case, shared lock on file is taken before
reading serialized content. If this second time fails again, it's not
because cache_set is interfering and so broken file is removed.

= Security Considerations

TODO: Fix permissions and move to code comment.

Usually filecache cleans its cache with credentials of web server.
When using Drush and the latter tries to clean cache, this operation
is run with user's credentials that are generally different than web
server's credentials.  For this reason, filecache creates all files
and directories with read/write permissions for all.

= Ideas for improvement

* Add fast page cache mode, see _drupal_bootstrap_page_cache().
* Implement session and path storage.
